type JSON = ApiEventPayload

export interface ApiEventPayload {
  resource: string;
  path: string;
  httpMethod: string;
  headers: Headers;
  multiValueHeaders: MultiValueHeaders;
  queryStringParameters: QueryStringParameters;
  multiValueQueryStringParameters: MultiValueQueryStringParameters;
  pathParameters: any;
  stageVariables: any;
  requestContext: RequestContext;
  body: any;
  isBase64Encoded: boolean;
}

interface Headers {
  Accept: string;
  "Accept-Encoding": string;
  "CloudFront-Forwarded-Proto": string;
  "CloudFront-Is-Desktop-Viewer": string;
  "CloudFront-Is-Mobile-Viewer": string;
  "CloudFront-Is-SmartTV-Viewer": string;
  "CloudFront-Is-Tablet-Viewer": string;
  "CloudFront-Viewer-Country": string;
  Host: string;
  "Postman-Token": string;
  "User-Agent": string;
  Via: string;
  "X-Amz-Cf-Id": string;
  "X-Amzn-Trace-Id": string;
  "X-Forwarded-For": string;
  "X-Forwarded-Port": string;
  "X-Forwarded-Proto": string;
}

interface MultiValueHeaders {
  Accept: string[];
  "Accept-Encoding": string[];
  "CloudFront-Forwarded-Proto": string[];
  "CloudFront-Is-Desktop-Viewer": string[];
  "CloudFront-Is-Mobile-Viewer": string[];
  "CloudFront-Is-SmartTV-Viewer": string[];
  "CloudFront-Is-Tablet-Viewer": string[];
  "CloudFront-Viewer-Country": string[];
  Host: string[];
  "Postman-Token": string[];
  "User-Agent": string[];
  Via: string[];
  "X-Amz-Cf-Id": string[];
  "X-Amzn-Trace-Id": string[];
  "X-Forwarded-For": string[];
  "X-Forwarded-Port": string[];
  "X-Forwarded-Proto": string[];
}

interface QueryStringParameters {
  morestuff: string;
  stuff: string;
}

interface MultiValueQueryStringParameters {
  morestuff: string[];
  stuff: string[];
}

interface RequestContext {
  resourceId: string;
  resourcePath: string;
  httpMethod: string;
  extendedRequestId: string;
  requestTime: string;
  path: string;
  accountId: string;
  protocol: string;
  stage: string;
  domainPrefix: string;
  requestTimeEpoch: number;
  requestId: string;
  identity: Identity;
  domainName: string;
  apiId: string;
}

interface Identity {
  cognitoIdentityPoolId: any;
  accountId: any;
  cognitoIdentityId: any;
  caller: any;
  sourceIp: string;
  principalOrgId: any;
  accessKey: any;
  cognitoAuthenticationType: any;
  cognitoAuthenticationProvider: any;
  userArn: any;
  userAgent: string;
  user: any;
}
