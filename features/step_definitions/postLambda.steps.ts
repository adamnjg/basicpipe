import { HttpMethods } from "../../infrastructure/builders/HttpMethods";
import { ApiStackBuilder } from "../../infrastructure/builders/ApiStackBuilder";
import { binding, given, then, when} from 'cucumber-tsflow';
import { App, Stack } from "aws-cdk-lib";
import { Template } from "aws-cdk-lib/assertions";
import { SettingDto } from "../../Models/SettingDto";
import { SettingsDto } from "../../Models/SettingsDto";
import { Guid } from "guid-typescript";

const app = new App();
const httpMethods: string[] = [HttpMethods.POST, HttpMethods.GET, HttpMethods.PUT, HttpMethods.DELETE];

const apiStack: Stack = new ApiStackBuilder(app, 'Settings', httpMethods);
const template: Template  = Template.fromStack(apiStack);

@binding()
export class ApiStackSteps {

    settings : SettingsDto;

    @given(/a settings object/)
    public eventContainingSettingsObject() {
        let settingList : SettingDto[] = [];
        settingList.push(new SettingDto("setting 1", "true"));
        settingList.push(new SettingDto("setting 2", "true"));
        settingList.push(new SettingDto("setting 3", "true"));
        this.settings = new SettingsDto(Guid.create(), settingList);
    }

    @when(/the settings object is passed to the lambda/)
    public runLambdaWithSettingsObject() {
        return true
    }

    @then(/the object is in the DynamoDB/)
    public checkDynamoDB() {
        return true
    }
}