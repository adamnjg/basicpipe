

export class HttpMethods {
    public static readonly POST : string = "POST";
    public static readonly GET : string = "GET";
    public static readonly PUT : string = "PUT";
    public static readonly DELETE : string = "DELETE";
}